#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
# __author__ = 'Davide "argaar" Foschi'
#
# ######################################################################################
# #
# # Check CommVault Simpana Backup Status
# #
# # Purpose: Check Result of Simpana backup jobs.
# # Due to poor software specs, we found no entry point like api or rest to
# # check for job exec status.
# # So instead the only way is to ask for report data and iterate throught it's values
# #
# # Be aware that the script is supposed to work in an Active Direcotry Environment
# # since it'll performa NTLM auth against Simpana WebPage
# #
# # License: CC BY-ND 3.0 (http://creativecommons.org/licenses/by-nd/3.0/)
# #
# #######################################################################################
#
# ver 1.5

import requests, argparse, sys
from requests.auth import HTTPBasicAuth
import json

parser = argparse.ArgumentParser(description='Check backup jobs, more info at: https://gitlab.com/argaar/nagios-plugins')
parser.add_argument('--H', metavar='host', help='(required) Simpana Web Page', required=True)
parser.add_argument('--U', metavar='user', help='(required) NTLM Username', required=True)
parser.add_argument('--P', metavar='pass', help='(required) NTLM Password', required=True)
parser.add_argument('--p', metavar='port', default='80', help='(optional) Webserver Port (default 80)')
parser.add_argument('--c', metavar='client', help='(required) Client Name to lookup status', required=True)
parser.add_argument('--d', metavar='datasetid', help='(required) DataSet ID (retrieved by intercepting xhr)', required=True)
parser.add_argument('--t', metavar='runtime', help='(optional) Max Running time (minutes) for a Job')
parser.add_argument('--f', metavar='timeframe', default='24', help='(optional) Get Backups data from the past XXX hours (default 24)')
parser.add_argument('--l', metavar='limit', default='50', help='(optional) Limit results to N (default 50)')
parser.add_argument('--v', action='version', version='%(prog)s 1.5')
args = parser.parse_args()

hostname = args.H+':'+args.p
loginUrl = 'http://'+hostname+'/webconsole/login/'
ssoUrl = 'http://'+hostname+'/webconsole/ssoLogin.do'
dataUrl = 'http://'+hostname+'/webconsole/proxy/cr/reportsplusengine/datasets/'+args.d+'/data/'
username = args.U
password = args.P
clientName = args.c.lower()
timeframe = args.f
joblimit = args.l

session = requests.Session()
session.auth = HTTPBasicAuth(username,password)

loginRes = session.get(loginUrl)

csrf_cookie = session.cookies['csrf']
jsess_cookie = session.cookies['JSESSIONID']

cookies_s1 = {
    'JSESSIONID': jsess_cookie,
    'csrf': csrf_cookie
}

headers_s1 = {
    'Origin': 'http://'+hostname,
    'Accept-Encoding': 'gzip, deflate',
    'X-CSRF-Token': csrf_cookie,
    'Accept-Language': 'en-US;q=0.9,en;q=0.8',
    'X-Requested-With': 'XMLHttpRequest',
    'Connection': 'keep-alive',
    'Pragma': 'no-cache',
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'Accept': '*/*',
    'Cache-Control': 'no-cache',
    'Referer': 'http://'+hostname+'/webconsole/login/',
    'DNT': '1',
}

data_s1 = [
  ('stayLoggedIn', '0'),
]

ssoRes = session.post(ssoUrl, headers=headers_s1, cookies=cookies_s1, data=data_s1)

if ssoRes.status_code == 200:
    login_cookie = ssoRes.cookies['LOGIN_COOKIE']

    cookies_s2 = {
        'JSESSIONID': jsess_cookie,
        'csrf': csrf_cookie,
        'LOGIN_COOKIE': login_cookie
    }

    headers_s2 = {
        'Connection': 'keep-alive',
        'Pragma': 'no-cache',
        'Cache-Control': 'no-cache',
        'Host': args.H,
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
        'Accept': 'application/json, text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate',
        'Accept-Language': 'en-US;q=0.9,en;q=0.8',
        'X-CSRF-Token': csrf_cookie
    }

    params_s2 = (
        ('fields', '[JobId] AS [JobId],[Client] AS [Client],[Duration(mins)] AS [Durationmins],[Job Status] AS [JobStatus], [Start Time] AS [StartTime],[End Time] AS [EndTime]'),
        ('orderby', '[JobId] Desc'),
        ('componentName', 'Job Details'),
        ('parameter.timeframe', '-PT'+timeframe+'H P0D'),
        ('parameter.useCSTimeZone', '1'),
        ('parameter.WindowStartTime', '00:00:00'),
        ('limit', joblimit)
    )

    dataRes = session.get(dataUrl, headers=headers_s2, params=params_s2, cookies=cookies_s2).json()
    if 'errorCode' in dataRes:
        print 'CRITICAL - Error querying data: '+dataRes['errorMessage']
        sys.exit(2)

    for job in dataRes['records']:
        name = job[1]
        runtime = job[2]
        result = job[3]
        endtime = job[5]
        if (name.lower() == clientName):
            if (result == 'Completed'):
                print 'OK - Result: ' + result + ', RunTime: ' + str(runtime) + 'm, EndTime: ' + str(endtime) + ' UTC'
                exit(0)
            elif (result == 'Failed'):
                print 'CRITICAL - Job Failed, Result: ' + result + ', RunTime: ' + str(runtime) + 'm, EndTime: ' + str(endtime) + ' UTC'
                exit(2)
            elif (result == 'Running'):
                if (args.t and int(runtime)>int(args.t)):
                    print 'WARNING - Job runtime is over max, Result: ' + result + ', RunTime: ' + str(runtime) + 'm, EndTime: ' + str(endtime) + ' UTC'
                    exit(1)
                else:
                    print 'UNKNOWN - Job still running, Result: ' + result + ', RunTime: ' + str(runtime) + 'm, EndTime: ' + str(endtime) + ' UTC'
                    exit(3)
            else:
                print 'WARNING - Unhandled job status, Result: ' + result + ', RunTime: ' + str(runtime) + 'm, EndTime: ' + str(endtime) + ' UTC'
                exit(1)

    print 'UNKNOWN - No Backup Job found for client in the last ' + str(timeframe) + 'H'
    sys.exit(3)
else:
    print 'CRITICAL - Cannot Login, maybe the credentials supplied are invalid?'
    sys.exit(2)